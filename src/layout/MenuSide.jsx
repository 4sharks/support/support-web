import React from 'react' 
import { NavLink } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
// React Icons
import {MdOutlineAddBox} from 'react-icons/md'
import {TbFileInvoice} from 'react-icons/tb'
import {FiSettings} from 'react-icons/fi'
import {AiOutlineDashboard} from 'react-icons/ai'
import {LuLayoutDashboard} from 'react-icons/lu'
import {HiOutlineDocumentReport} from 'react-icons/hi'
import {PiUsersThreeThin} from 'react-icons/pi'
import {LiaUsersCogSolid,LiaUsersSolid} from 'react-icons/lia'
import {RiQuoteText} from 'react-icons/ri'

// Custom hooks
import { useTenant } from '../hooks'

const MenuSide = ({openMenu,setOpenMenu}) => {
    const [t] = useTranslation('global')
    const {tenantUsername} = useTenant();
    return (
        <div 
            // onMouseOver={()=>setOpenMenu(true)}
            className={`  z-50   flex items-center   transition-all duration-500  ease-out  h-screen      primary-bg-color border-b    ${openMenu ? 'fixed px-3 w-9 flex md:w-40 md:relative ' : 'fixed w-0 hidden md:flex md:w-12 md:px-3  '}`}>
                <ul className=' cursor-default  flex  flex-col gap-5 items-center  text-white  text-xs  '>
                    <li className={`w-full  menu-border-color   border-b pb-2 hover:text-yellow-400 ${ openMenu || 'justify-center' } `}>
                        <NavLink to={`/${tenantUsername}`} className={`flex items-center gap-2 `} end >
                            <AiOutlineDashboard size={20}  title={t('menu.Dashboard')} />
                            { openMenu && <span className='whitespace-nowrap hidden md:flex'>{t('menu.Dashboard')}</span>}
                        </NavLink>
                    </li>
                    <li className={`w-full menu-border-color border-b pb-2 hover:text-yellow-400 ${ openMenu || 'justify-center' } `}>
                        <NavLink to={`/${tenantUsername}/tickets/add`} className={`flex items-center gap-2 `} end >
                            <MdOutlineAddBox size={20} title="انشاء تذكرة دعم فني" />
                            { openMenu && <span className='whitespace-nowrap hidden md:flex'>انشاء تذكرة دعم فني</span>}
                        </NavLink>
                    </li>
                    <li className={` w-full gap-2 flex items-center  menu-border-color  border-b pb-2 hover:text-yellow-400 ${ openMenu || 'justify-center' } `}>
                        <NavLink to={`/${tenantUsername}/tickets`} className={`flex items-center gap-2 `} end >
                            <TbFileInvoice size={20} title={`تذاكر الدعم الفني`}/>
                            { openMenu && <span className='whitespace-nowrap hidden md:flex'>{`تذاكر الدعم الفني`}</span>}
                        </NavLink>
                    </li>
                  
                   
                    
                </ul>
        </div>
    )
}

export default MenuSide