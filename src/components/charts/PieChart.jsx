import {
    Chart as ChartJS,
    ArcElement,
    Tooltip,
    Legend
} from 'chart.js';
import {Doughnut } from 'react-chartjs-2'
import { MwSpinnerButton } from '../ui';

ChartJS.register(
    ArcElement,
    Tooltip,
    Legend
)

const PieChart = ({labels,datasetsLabel,datasetsData,borderColor,backgroundColor,className,loading=false}) => {
    const data = {
        labels: labels,
        datasets:[
            {
                label:datasetsLabel ,
                data:datasetsData,
                borderColor: borderColor,
                backgroundColor: backgroundColor,
                tension:0.4,
                
                // circumference:180,
                // rotation:270
            }
        ]
    }
    const options = {}

    return ( 
        <div className='rounded-lg bg-slate-50 p-3 w-full md:w-fit border-2  border-slate-50'>
            { !loading ? 
                    <Doughnut 
                        className={` ${className}`}
                        data={data}
                        options={options}
                    >
                    </Doughnut>
                : <MwSpinnerButton withLabel={false} isFullCenter={true} />
            }
        </div> 
    )
}

export default PieChart