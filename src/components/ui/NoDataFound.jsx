import React from 'react'

const NoDataFound = ({msg}) => {
    return (
        <div className='text-sm text-slate-400 font-bold text-center p-5 md:p-7 md:m-7'>{msg}</div>
    )
}

export default NoDataFound