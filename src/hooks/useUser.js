import { useSelector } from 'react-redux'

const useUser = () => {

    const getUser =  useSelector((state) => state.auth).user

    return {getUser}
}

export default useUser